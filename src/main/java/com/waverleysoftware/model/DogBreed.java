package com.waverleysoftware.model;

import com.waverleysoftware.behaviour.Breed;

public enum DogBreed implements Breed{
    OVCHARKA,
    HASKI
}
